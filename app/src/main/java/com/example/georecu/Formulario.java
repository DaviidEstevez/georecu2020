package com.example.georecu;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Formulario extends AppCompatActivity {

    private FirebaseDatabase database;
    private DatabaseReference databaseReference;

    private EditText etNombre, etLatitud, etLongitud, etDescripcion, etFecha;
    private Button btnGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference().child("georecu");


        etNombre = findViewById(R.id.etNombre);
        etLatitud = findViewById(R.id.etLatitud);
        etLongitud = findViewById(R.id.etLongitud);
        etDescripcion = findViewById(R.id.mtDescripcion);
        etFecha = findViewById(R.id.etFecha);

        btnGuardar= findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
                String strFecha = etFecha.getText().toString();
                try {
                    Date fecha = (Date) formatoDelTexto.parse(strFecha);
                    Lugar lugar = new Lugar(
                            etNombre.getText().toString(),
                            etLatitud.getText().toString(),
                            etLongitud.getText().toString(),
                            etDescripcion.getText().toString(),
                            fecha
                    );
                    databaseReference.push().setValue(lugar);

                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                finish();
            }
        });


    }
}
