package com.example.georecu;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private FirebaseDatabase database;
    private DatabaseReference databaseReference;

    private Button btnHideMark;
    private CardView infoContainer;
    private TextView tvNombreMark, tvFechaMark, etDescriptionMark, tvLatitudMark, tvLongitudMark;


    private double longitud;
    private double latitud;
    List<Lugar> lugares = new ArrayList<Lugar>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference().child("georecu");

        tvNombreMark = findViewById(R.id.tvNombreMark);
        tvFechaMark = findViewById(R.id.tvFechaMark);
        etDescriptionMark = findViewById(R.id.etDescriptionMark);
        tvLatitudMark = findViewById(R.id.tvLatitudMark);
        tvLongitudMark = findViewById(R.id.tvLongitudMark);

        infoContainer = findViewById(R.id.infoContainer);

        btnHideMark = findViewById(R.id.btnHideMark);
        btnHideMark.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                infoContainer.setVisibility(View.INVISIBLE);
            }
        });



        generarMarcadores();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

    }

    public void mapita(double latitud, double longitud) {

        LatLng sydney = new LatLng(latitud, longitud);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                infoContainer.setVisibility(View.VISIBLE);

                for (Lugar lugar: lugares) {
                    if((new LatLng(Double.parseDouble(lugar.getLatitud()),(Double.parseDouble(lugar.getLongitud())))).equals(marker.getPosition())){
                        tvNombreMark.setText(lugar.getNombre());
                        etDescriptionMark.setText(lugar.getDescripcion());

                        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");


                        tvFechaMark.setText(formato.format(lugar.getFecha()));
                        tvLatitudMark.setText(lugar.getLatitud());
                        tvLongitudMark.setText(lugar.getLongitud());
                    }
                }
                return false;
            }
        });
    }

    private void generarMarcadores() {

        databaseReference.orderByChild("georecu").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getChildrenCount() > 0) {
                            for (DataSnapshot dataSnap : dataSnapshot.getChildren()) {

                                String nombre = dataSnap.child("nombre").getValue().toString();
                                String descripcion = dataSnap.child("descripcion").getValue().toString();
                                String fecha = dataSnap.child("fecha").child("time").getValue().toString();
                                Long milis = Long.parseLong(fecha);
                                String latitudS =dataSnap.child("latitud").getValue().toString();
                                String longitudS =dataSnap.child("longitud").getValue().toString();



                                latitud = Double.parseDouble(latitudS);
                                longitud = Double.parseDouble(longitudS);

                                lugares.add(new Lugar(nombre, latitudS, longitudS, descripcion, new Date(milis)));

                                mapita(latitud, longitud);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {}
                });
    }
}
